package io.github.nmhillusion.readnews.Setting;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.github.nmhillusion.readnews.MainActivity;

/**
 * Created by nmhillusion on 07 - June - 2017.
 */

public final class ResourceManager {
    private final static ResourceManager instance = new ResourceManager();

    private SharedPreferences sharedPreferences;

    private ResourceManager(){}

    public static ResourceManager getInstance(){
        return instance;
    }

    public Map<String, String> getResources(MainActivity mainActivity){
        Map<String, String> res = new HashMap<>();

        Set<String> resourceSet = mainActivity.getPreferences(Context.MODE_PRIVATE)
                .getStringSet("resourceNews", new HashSet<String>());

        for(String iResource : resourceSet){
            String[] item = iResource.split("\\|\\|");
            res.put(item[1], item[0]);
        }

        return res;
    }

    /**
     * update value to database
     * @param mainActivity main activity
     * @param resource resource will be update
     * @return update successful?
     */
    public boolean updateResource(MainActivity mainActivity, Map<String, String> resource){
        final Set<String> resourceSet = new HashSet<>();

        for(String key : resource.keySet()){
            resourceSet.add(resource.get(key) + "||" + key);
        }

        mainActivity.getPreferences(Context.MODE_PRIVATE)
                .edit()
                .putStringSet("resourceNews", resourceSet)
                .apply();

        return true;
    }
}
