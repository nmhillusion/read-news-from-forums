package io.github.nmhillusion.readnews.ReadResource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by nmhillusion on 6/6/17.
 */

/**
 * get all items from xml string
 */
final class ItemNewsGetter {
    private final static ItemNewsGetter mInstance = new ItemNewsGetter();

    private ItemNewsGetter(){}

    static ItemNewsGetter getInstance(){return mInstance;}

    List<ItemNews> parse(Map<String, String> source){
        List<ItemNews> res = new ArrayList<>();

        for (String iSource : source.keySet()) {
            Document document = XMLDomParser.getInstance().getDocument(source.get(iSource));

            NodeList nodeList = document.getElementsByTagName("item");

            for(short i = 0, length = (short)nodeList.getLength(); i < length; ++i){
                Element element = (Element)nodeList.item(i);
                Element title       = (Element) element.getElementsByTagName("title").item(0),
                        description = (Element) element.getElementsByTagName("description").item(0),
                        link        = (Element) element.getElementsByTagName("link").item(0),
                        time        = (Element) element.getElementsByTagName("pubDate").item(0),
                        author      = (Element) element.getElementsByTagName("author").item(0);
                ItemNews item = new ItemNews();
                if(title!=null){item.setTitle(title.getTextContent());}
                if(description!=null){item.setDescription(description.getTextContent());}
                if(link!=null){item.setLink(link.getTextContent());}
                if(time!=null){item.setTime(time.getTextContent());}
                if(author!=null){item.setAuthor(author.getTextContent());}
                item.setSource(iSource);
                res.add(item);
            }
        }

        return res;
    }
}