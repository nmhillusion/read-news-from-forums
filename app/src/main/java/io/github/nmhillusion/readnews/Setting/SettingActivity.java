package io.github.nmhillusion.readnews.Setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.github.nmhillusion.readnews.DialogPackage.Dialog;
import io.github.nmhillusion.readnews.DialogPackage.OnClickedDialogListener;
import io.github.nmhillusion.readnews.R;

/**
 * Created by nmhillusion on 07 - June - 2017.
 */

public final class SettingActivity extends Activity {
    private ListView listView;
    private ArrayList<String> arrayList;
    private ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        if(getIntent().getBooleanExtra("isSettingResource", true)){
            findViewById(R.id.tab_color).setVisibility(View.GONE);
            listView = (ListView) findViewById(R.id.list_view_resource);
            arrayList = getIntent().getStringArrayListExtra("list-resource");

            arrayAdapter = new ArrayAdapter<>(
                    this,
                    R.layout.item_news_resource,
                    arrayList);
            listView.setAdapter(arrayAdapter);
            setTitle(getTitle() + " - News Resource");

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    showPopup(view, position);
                }
            });

            Intent intentResult = new Intent();
            intentResult.putStringArrayListExtra("resource", arrayList);
            setResult(RESULT_OK, intentResult);
        }else{
            findViewById(R.id.tab_resource).setVisibility(View.GONE);
            setTitle(getTitle() + " - Background Color");
        }
    }

    private void showPopup(View view, final int position){
        PopupMenu popup = new PopupMenu(this, view);
        popup.getMenuInflater().inflate(R.menu.popup_menu_setting_resource, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.edit_news_resource:
                        editNewsResource(position);
                        break;

                    case R.id.delete_news_resource:
                        deleteNewsResource(position);
                        break;
                }

                return false;
            }
        });

        popup.show();
    }

    private void editNewsResource(final int position){
        List<Pair<String, String>> input = new ArrayList<Pair<String, String>>();
        input.add(new Pair<>("Title of Resource", arrayList.get(position).split(" \\|\\| ")[0]));
        input.add(new Pair<>("Link of Resource", arrayList.get(position).split(" \\|\\| ")[1]));

        Dialog.getInstance().prompt(
                SettingActivity.this,
                null,
                input,
                null,
                new OnClickedDialogListener() {
                    @Override
                    public void onClick(@Nullable Boolean isPossitive) {}

                    @Override
                    public void onClickForResult(@Nullable final List<String> result) {
                        if(result!=null && checkEditResource(result))
                        {
                            Dialog.getInstance().confirm(SettingActivity.this,
                                    "Do you confirm to apply new value?",
                                    new OnClickedDialogListener() {
                                        @Override
                                        public void onClick(@Nullable Boolean isPossitive) {
                                            if(isPossitive != null && isPossitive){
                                                arrayList.set(position,result.get(0) + " || " + result.get(1));
                                                arrayAdapter.notifyDataSetChanged();
                                            }
                                        }

                                        @Override
                                        public void onClickForResult(@Nullable List<String> result) {}
                                    });
                        }
                    }
                }
        );
    }

    private boolean checkEditResource(List<String> input){
        for(String s : input){
            if(s.contains("||")) {
                Dialog.getInstance().alert(this, "value can not contain \"||\"", null);
                return false;
            }
        }
        return true;
    }

    private void deleteNewsResource(final int position){
        Dialog.getInstance().confirm(
                this,
                "Do you sure to want to delete: " + arrayList.get(position),
                new OnClickedDialogListener() {
                    @Override
                    public void onClick(@Nullable Boolean isPossitive) {
                        if(isPossitive!=null && isPossitive){
                            arrayList.remove(position);
                            arrayAdapter.notifyDataSetChanged();
                            Toast.makeText(SettingActivity.this, "Deleted!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onClickForResult(@Nullable List<String> result) {}
                }
        );
    }

    public void changeColor(View view){
        Intent intentResult = new Intent();
        intentResult.putExtra("color", view.getContentDescription().toString().replace("color", "#"));
        setResult(RESULT_OK, intentResult);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(getIntent().getBooleanExtra("isSettingResource", true)){
            getMenuInflater().inflate(R.menu.option_menu_setting_resource, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public void addNewResource(MenuItem view){
        List<Pair<String, String>> input = new ArrayList<Pair<String, String>>();
        input.add(new Pair<>("Title of Resource", ""));
        input.add(new Pair<>("Link of Resource", ""));

        Dialog.getInstance().prompt(
                this,
                null,
                input,
                "input something...",
                new OnClickedDialogListener() {
                    @Override
                    public void onClick(@Nullable Boolean isPossitive) {}

                    @Override
                    public void onClickForResult(final @Nullable List<String> result) {
                        if(result!=null && checkEditResource(result))
                        {
                            Dialog.getInstance().confirm(SettingActivity.this,
                                    "Do you confirm to add new resource?",
                                    new OnClickedDialogListener() {
                                        @Override
                                        public void onClick(@Nullable Boolean isPossitive) {
                                            if(isPossitive != null && isPossitive){
                                                arrayList.add(result.get(0) + " || " + result.get(1));
                                                arrayAdapter.notifyDataSetChanged();
                                            }
                                        }

                                        @Override
                                        public void onClickForResult(@Nullable List<String> result) {}
                                    });
                        }
                    }
                }
        );
    }
}