package io.github.nmhillusion.readnews.DialogPackage;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nmhillusion on 09 - June - 2017.
 */

public final class Dialog {
    private final static Dialog instance = new Dialog();

    private Dialog(){}

    public static Dialog getInstance(){
        return instance;
    }

    public void alert(Context context, String text, final OnClickedDialogListener listener){
        new AlertDialog.Builder(context)
                .setMessage(text)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(listener!=null)listener.onClick(null);
                    }
                })
                .create()
                .show();
    }

    public void confirm(Context context, String text, @NonNull final OnClickedDialogListener listener){
        new AlertDialog.Builder(context)
                .setMessage(text)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onClick(true);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onClick(false);
                    }
                })
                .create()
                .show();
    }

    public void prompt(final Context context, @Nullable Integer backgroundTextId, final List<Pair<String, String>> input, @Nullable String hint, @NonNull final OnClickedDialogListener listener){
        LinearLayout container = new LinearLayout(context);
        container.setOrientation(LinearLayout.VERTICAL);
        container.setPadding(15,15,15,15);

        final int size = input.size();
        final TextView[] textView = new TextView[size];
        final EditText[] editText = new EditText[size];

        for(int i = 0; i< size;i++){
            textView[i] = new TextView(context);
            textView[i].setText(input.get(i).first);
            textView[i].setTextColor(Color.parseColor("#359bed"));
            textView[i].setTextSize(15);

            editText[i] = new EditText(context);
            editText[i].setText(input.get(i).second);

            container.addView(textView[i]);container.addView(editText[i]);
        }

        new AlertDialog.Builder(context)
                .setView(container)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        List<String> value = new ArrayList<String>();

                        String temp = "";
                        for(int j= 0; j< size; ++j){
                            temp = editText[j].getText().toString();
                            if(temp.length() < 1){
                                alert(context, "value of "+input.get(j).first+" can not null", null);
                                return;
                            }else{
                                value.add(temp);
                            }
                        }
                        listener.onClickForResult(value);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onClickForResult(null);
                    }
                })
                .create()
                .show();
    }
}