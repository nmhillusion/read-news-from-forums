package io.github.nmhillusion.readnews.ItemListGUI;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import io.github.nmhillusion.readnews.MainActivity;
import io.github.nmhillusion.readnews.R;
import io.github.nmhillusion.readnews.ReadResource.ItemNews;


/**
 * Created by nmhillusion on 6/6/17.
 */

public final class NewsAdapter extends BaseAdapter{
    private List<ItemNews> listNews;
    private Context context;
    private int layout;

    public NewsAdapter(List<ItemNews> listNews, Context context, int layout) {
        Collections.shuffle(listNews);
        this.listNews = listNews;
        this.context = context;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return listNews.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHandler{
        TextView title, description, author, time, source;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewHandler viewHandler = new ViewHandler();
        if (convertView == null) {
            convertView = layoutInflater.inflate(layout, null);

            viewHandler.title = (TextView) convertView.findViewById(R.id.item_news_title);
            viewHandler.description = (TextView) convertView.findViewById(R.id.item_news_description);
            viewHandler.author = (TextView) convertView.findViewById(R.id.item_news_author);
            viewHandler.time = (TextView) convertView.findViewById(R.id.item_news_time);
            viewHandler.source = (TextView) convertView.findViewById(R.id.item_news_source);

            convertView.setTag(viewHandler);
        } else {
            viewHandler = (ViewHandler) convertView.getTag();
        }

        ItemNews news = listNews.get(position);
        viewHandler.title.setText(news.getTitle());
        viewHandler.description.setText(news.getDescription().replaceAll("<(.*?)>", ""));

        if(news.getAuthor() != null){
            viewHandler.author.setText("author: " + news.getAuthor());
        }
        viewHandler.time.setText(news.getTime());
        viewHandler.source.setText("source: " + MainActivity.getTitleSource(news.getSource()));

        String colorString = context.getSharedPreferences("color_background", Context.MODE_PRIVATE).getString("color", "#839ea1");
        int color = Color.parseColor(colorString);
        viewHandler.source.setBackgroundColor(color);

        convertView.findViewById(R.id.footerNews).setBackgroundColor(color);

        Animation animation = AnimationUtils.loadAnimation(context, R.anim.item_news_anim);
        convertView.startAnimation(animation);

        return convertView;
    }
}