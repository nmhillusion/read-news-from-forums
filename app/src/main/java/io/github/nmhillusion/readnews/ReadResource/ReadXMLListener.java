package io.github.nmhillusion.readnews.ReadResource;

import java.util.List;

/**
 * Created by nmhillusion on 6/6/17.
 */

public interface ReadXMLListener {
    void onReceived(List<ItemNews> result);
    void onTickLoaded(int now, int sum, String loadLabel);
}
