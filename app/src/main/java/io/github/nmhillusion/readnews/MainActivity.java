package io.github.nmhillusion.readnews;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.github.nmhillusion.readnews.ItemListGUI.NewsAdapter;
import io.github.nmhillusion.readnews.NewsViewer.WebViewerActivity;
import io.github.nmhillusion.readnews.ReadResource.ItemNews;
import io.github.nmhillusion.readnews.ReadResource.ReadXMLListener;
import io.github.nmhillusion.readnews.ReadResource.Retriever;
import io.github.nmhillusion.readnews.Setting.ResourceManager;
import io.github.nmhillusion.readnews.Setting.SettingActivity;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private List<ItemNews> listNews;
    private NewsAdapter newsAdapter;
    private ProgressBar progressBar;
    private TextView lblProgress;
    private static Map<String, String> newsResource;

    private final short REQUEST_CODE_BACKGROUND_COLOR = 123,
                        REQUEST_CODE_NEWS_RESOURCE = 234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listNews);
        progressBar = (ProgressBar) findViewById(R.id.progressBar_load_all);
        progressBar.setMax(100);
        progressBar.setVisibility(View.VISIBLE);
        lblProgress = (TextView) findViewById(R.id.progress_title);

        if(ResourceManager.getInstance().getResources(this).isEmpty()) {
            Map<String, String> demo = new HashMap<>();
            demo.put("https://tinhte.vn/rss", "Tinh Tế");
            demo.put("https://vinacode.net/feed", "VinaCode");
            demo.put("https://vozforums.com/external.php?type=RSS2&forumids=33", "vozForums F33");
            demo.put("http://vnexpress.net/rss/thoi-su.rss", "VnExpress Thời Sự");
            demo.put("http://vnexpress.net/rss/so-hoa.rss", "VnExpress  Số Hóa");
            demo.put("http://vnexpress.net/rss/the-gioi.rss", "VnExpress Thế Giới");
            demo.put("http://vnexpress.net/rss/du-lich.rss", "VnExpress Du Lich");
            demo.put("http://feeds.dzone.com/home", "DZone");
            demo.put("https://news.google.com/news?cf=all&hl=vi&pz=1&ned=us&output=rss", "Google News VN");
            demo.put("https://news.google.com/news?cf=all&hl=en&pz=1&ned=us&output=rss", "Google News US");
            ResourceManager.getInstance().updateResource(this, demo);
        }

        updateResourceAndDisplay();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, WebViewerActivity.class);
                intent.putExtra("url", listNews.get(position).getLink());
                startActivity(intent);
            }
        });
    }

    private void updateResourceAndDisplay(){
        listView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        lblProgress.setVisibility(View.VISIBLE);

        newsResource = ResourceManager.getInstance().getResources(this);
        if(!newsResource.isEmpty()){
            lblProgress.setText("loading: " + newsResource.values().toArray()[0].toString());
        }
        Retriever.getInstance().getContentXML(new ReadXMLListener() {
            @Override
            public void onReceived(List<ItemNews> result) {
                listNews = result;
                newsAdapter = new NewsAdapter(listNews, MainActivity.this, R.layout.item_news);
                listView.setAdapter(newsAdapter);
                progressBar.setVisibility(View.GONE);
                lblProgress.setVisibility(View.GONE);

                listView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTickLoaded(int now, int sum, String label) {
                progressBar.setProgress(100*now/sum);
                lblProgress.setText(label);
            }
        }, newsResource.keySet());
    }

    public static String getTitleSource(String query){
        return newsResource.get(query);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_option, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.setting_resource:
                Intent intent = new Intent(this, SettingActivity.class);
                intent.putExtra("isSettingResource", true);
                intent.putStringArrayListExtra("list-resource", getListResource());
                startActivityForResult(intent, REQUEST_CODE_NEWS_RESOURCE);
                break;
            case R.id.setting_color:
                Intent intentc = new Intent(this, SettingActivity.class);
                intentc.putExtra("isSettingResource", false);
                startActivityForResult(intentc, REQUEST_CODE_BACKGROUND_COLOR);
                break;
            case R.id.filter:
                Toast.makeText(this, "Filter", Toast.LENGTH_SHORT).show();
                break;
            case R.id.about_me:
                startActivity(new Intent(this, AboutMeActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private ArrayList<String> getListResource(){
        ArrayList<String> list = new ArrayList<>();
        for(String key : newsResource.keySet()){
            list.add(newsResource.get(key) + " || " + key);
        }
        return list;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if(requestCode== REQUEST_CODE_BACKGROUND_COLOR){
                getSharedPreferences("color_background", MODE_PRIVATE).edit().putString("color", data.getStringExtra("color")).apply();
                newsAdapter.notifyDataSetChanged();
            }else{
                ResourceManager.getInstance()
                        .updateResource(this, listString2Map(data.getStringArrayListExtra("resource")));

                updateResourceAndDisplay();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private Map<String, String> listString2Map(List<String> input){
        final Map<String, String> resource = new LinkedHashMap<>();
        for(String r : input){
            String[] str = r.split(" \\|\\| ");
            resource.put(str[1], str[0]);
        }

        return resource;
    }
}