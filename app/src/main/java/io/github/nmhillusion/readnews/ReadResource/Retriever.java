package io.github.nmhillusion.readnews.ReadResource;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import io.github.nmhillusion.readnews.MainActivity;

/**
 * Created by nmhillusion on 6/6/17.
 */

public final class Retriever {
    private final static Retriever mInstance = new Retriever();

    private ReadXMLListener listener;

    private Retriever(){}

    public static Retriever getInstance(){return mInstance;}

    public void getContentXML(ReadXMLListener listener, Collection url){
        this.listener = listener;
        new ReadXML().execute(url.toArray());

    }

    private class ReadXML extends AsyncTask<Object, Object, Map<String, String>>{
        @Override
        protected Map<String, String> doInBackground(Object... links) {
            Map<String, String> result = new HashMap<>();
            StringBuilder temp = null;
            for (int i=0; i< links.length; ++i) {
                try {
                    temp = new StringBuilder();
                    URLConnection connect = new URL(links[i].toString()).openConnection();
                    BufferedReader br = new BufferedReader(new InputStreamReader(connect.getInputStream()));

                    String line;
                    while((line=br.readLine())!=null){
                        temp.append(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if(temp.toString().length()>10)result.put(links[i].toString(), temp.toString());
                publishProgress(i+1, links.length, "loading: " + MainActivity.getTitleSource(links[i==links.length-1?0:i+1].toString()));
            }

            return result;
        }

        protected void onProgressUpdate(Object... progress) {
            listener.onTickLoaded(Integer.valueOf(progress[0].toString()),
                    Integer.valueOf(progress[1].toString()),
                    progress[2].toString()
                    );
        }

        @Override
        protected void onPostExecute(Map<String, String> result) {
            super.onPostExecute(result);
            listener.onReceived(ItemNewsGetter.getInstance().parse(result));
        }
    }
}