package io.github.nmhillusion.readnews.NewsViewer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;

import io.github.nmhillusion.readnews.R;

/**
 * Created by nmhillusion on 06 - June - 2017.
 */

public class WebViewerActivity extends Activity{
    private WebView webView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        Intent intent = getIntent();

        webView = (WebView) findViewById(R.id.webview_news);
        webView.loadUrl(intent.getStringExtra("url"));

        final ProgressBar progressBar = findViewById(R.id.load_page_web_view);

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                setTitle("Watching News: " + webView.getTitle());
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);



        Button btnBack = new Button(this);
        btnBack.setText("back");
        getActionBar().setCustomView(btnBack);
    }
}