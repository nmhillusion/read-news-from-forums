package io.github.nmhillusion.readnews.ReadResource;

/**
 * Created by nmhillusion on 6/6/17.
 */

public final class ItemNews {
    private String title, description, time, link, author, source;

    public ItemNews() {
    }

    public ItemNews(String title, String description, String time, String link, String author, String source) {
        this.title = title;
        this.description = description;
        this.time = time;
        this.link = link;
        this.author = author;
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getTime() {
        return time;
    }

    public String getLink() {
        return link;
    }

    public String getAuthor() {
        return author;
    }

    public String getSource(){
        return source;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return ":ItemNews:\n" + "title: " + title + "\ndescription: " + description + "\ntime: " + time
                + "\nlink: " + link + "\nauthor: " + author + "\nsource: " + source;
    }
}