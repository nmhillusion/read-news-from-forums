package io.github.nmhillusion.readnews.DialogPackage;

import android.support.annotation.Nullable;
import android.util.Pair;

import java.util.List;

/**
 * Created by nmhillusion on 09 - June - 2017.
 */

public interface OnClickedDialogListener {
    void onClick(@Nullable Boolean isPossitive);
    void onClickForResult(@Nullable List<String> result);
}
